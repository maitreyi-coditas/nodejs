const express = require("express")

const {people} = require("../data")

const router = express.router();
const {createPerson,createPersonPostman,updatePerson,getPeople,deletePerson} = require("../final/controllers/people")



// router.get('/',getPeople)
// router.post('/',createPerson)

// router.post('/',createPersonPostman)

// router.put('/:id',updatePerson)

// router.delete('/:id',deletePerson)

router.route("/").get(getPeople).post(createPerson);
router.route("/postman").post(createPersonPostman)
router.route("/:id").put(updatePerson).delete(deletePerson)

module.exports = router