const secret = "Super Secret";
const john = "john";
const peter = "Peter";

module.exports = { 
    john, 
    peter 
}

module.exports.items = ["item1", "item2"]

const person = {
    name: "bob",
}

module.exports.singlePerson = person