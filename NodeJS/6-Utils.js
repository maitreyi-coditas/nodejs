const names = require('./4. Export.js')
const sayHi = require('./3. FunctionVariable.js')
const data = require('./4. Export.js')

console.log(data)

sayHi(names.john)
sayHi(names.peter)
sayHi("Susan")

// {
//     john: 'john',
//     peter: 'Peter',
//     items: [ 'item1', 'item2' ],
//     singlePerson: { name: 'bob' }
//   }

// Hello there john
// Hello there Peter
// Hello there Susan

require('./export1.js')