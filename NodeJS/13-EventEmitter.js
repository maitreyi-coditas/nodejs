
const EventEmitter = require('events')

const customEmitter = new EventEmitter()

customEmitter.on('response', (name, id) => {

  console.log(`User name:${name}, id:${id}`)

})

customEmitter.on('response', () => {

  console.log('Hello! You have a response')

})

customEmitter.emit('response', 'Moanna', 24)


