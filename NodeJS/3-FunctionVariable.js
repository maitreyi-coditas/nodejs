//Modules
//Every js file is a module which creates a global object which is private to that file.

const mona = "Mona"

const sayHi = (name) => {
    console.log(`Hello there ${name}!`)
}

sayHi("Susan");
sayHi(mona);

module.exports = sayHi;

// Without exports keyword
const num1 = 5;
const num2 = 10;

function addValues(){
    console.log("The sum is ", num1+num2)
}

addValues();